from dataclasses import dataclass

from nmealib.datatypes import NmeaTime, NmeaLatitude, NmeaLongitude, NmeaFloat, NmeaDegree, NmeaInt, NmeaBytes
from nmealib.nmea import NmeaSentence, nmea_field


# Reference for NMEA sentences: https://gpsd.gitlab.io/gpsd/NMEA.html


@dataclass
class DBT(NmeaSentence):
    """
    DBT - Depth below transducer

           1   2 3   4 5   6 7
           |   | |   | |   | |
    $--DBT,x.x,f,x.x,M,x.x,F*hh<CR><LF>

    Field Number:

    1. Water depth, feet
    2. f = feet
    3. Water depth, meters
    4. M = meters
    5. Water depth, Fathoms
    6. F = Fathoms
    7. Checksum

    In real-world sensors, sometimes not all three conversions are reported.
    So you might see something like $SDDBT,,f,22.5,M,,F*cs

    depth is given in feet, meters and fathoms respectively
    """
    example_sentence = b"$SDDBT,166.65,f,050.79,M,027.77,F*3A\r\n"
    depth: NmeaFloat = nmea_field(3, b"%.2f")
    depth_feet: NmeaFloat = nmea_field(1, b"%.2f")
    depth_fathoms: NmeaFloat = nmea_field(5, b"%.2f")

    def set_depths(self, depth_meters):
        """
        Convenience method to set the depth in all units consistently.
        Entries (feet, fathoms) which were not present in the sentence are skipped.
        """
        self.depth = depth_meters
        if self.depth_feet:
            self.depth_feet = 3.28084 * depth_meters
        if self.depth_fathoms:
            self.depth_fathoms = 0.546807 * depth_meters


@dataclass
class DPT(NmeaSentence):
    """
    DPT - Depth of Water

           1   2   3   4
           |   |   |   |
    $--DPT,x.x,x.x,x.x*hh<CR><LF>

    Field Number:

    1. Water depth relative to transducer, meters
    2. Offset from transducer, meters
       positive means distance from transducer to water line
       negative means distance from transducer to keel
    3. Maximum range scale in use (NMEA 3.0 and above)
    4. Checksum
    """
    example_sentence = b'$SDDPT,8.0,-0.1,5000.0,*68'
    depth: NmeaFloat = nmea_field(1)
    offset: NmeaFloat = nmea_field(2)


@dataclass
class DRU(NmeaSentence):
    # TODO: deprecated... maybe remove
    # DoLog23, Doppler Log
    example_sentence = b'$PKDRU,0003.9,M,,,+000,*3E'


@dataclass
class GGA(NmeaSentence):
    """
    GGA - Global Positioning System Fix Data

           1         2       3 4        5 6 7  8   9  10 11 12 13  14   15
           |         |       | |        | | |  |   |   | |   | |   |    |
    $--GGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh<CR><LF>

    Field Number:

    1. UTC of this position report
    2. Latitude
    3. N or S (North or South)
    4. Longitude
    5. E or W (East or West)
    6. GPS Quality Indicator (non null)
         - 0 - fix not available,
         - 1 - GPS fix,
         - 2 - Differential GPS fix
               (values above 2 are 2.3 features)
         - 3 = PPS fix
         - 4 = Real Time Kinematic
         - 5 = Float RTK
         - 6 = estimated (dead reckoning)
         - 7 = Manual input mode
         - 8 = Simulation mode
    7. Number of satellites in use, 00 - 12
    8. Horizontal Dilution of precision (meters)
    9. Antenna Altitude above/below mean-sea-level (geoid) (in meters)
    10. Units of antenna altitude, meters
    11. Geoidal separation, the difference between the WGS-84 earth
        ellipsoid and mean-sea-level (geoid), "-" means mean-sea-level
        below ellipsoid
    12. Units of geoidal separation, meters
    13. Age of differential GPS data, time in seconds since last SC104
        type 1 or 9 update, null field when DGPS is not used
    14. Differential reference station ID, 0000-1023
    15. Checksum
    """
    example_sentence = b'$GPGGA,191410,4735.5634,N,00739.3538,E,1,04,4.4,351.5,M,48.0,M,,*45'
    time: NmeaTime = nmea_field(1, b'%02.0f%02.0f%02.0f')
    lat: NmeaLatitude = nmea_field(2)
    lon: NmeaLongitude = nmea_field(4)


@dataclass
class GLL(NmeaSentence):
    """
    GLL - Geographic Position - Latitude/Longitude

           1       2 3        4 5         6 7   8
           |       | |        | |         | |   |
    $--GLL,llll.ll,a,yyyyy.yy,a,hhmmss.ss,a,m,*hh<CR><LF>

    Field Number:

    1. Latitude
    2. N or S (North or South)
    3. Longitude
    4. E or W (East or West)
    5. UTC of this position
    6. Status A - Data Valid, V - Data Invalid
    7. FAA mode indicator (NMEA 2.3 and later)
    8. Checksum
    """
    example_sentence = b'$GPGLL,4735.5634,N,00739.3538,E,191410,A,A*4A'
    lat: NmeaLatitude = nmea_field(1)
    lon: NmeaLongitude = nmea_field(3)
    time: NmeaTime = nmea_field(5)


@dataclass
class GST(NmeaSentence):
    """
    GST - GPS Pseudorange Noise Statistics

                1    2 3 4 5 6 7 8   9
                |    | | | | | | |   |
    $ --GST,hhmmss.ss,x,x,x,x,x,x,x,*hh<CR><LF>

    Field Number:

    1. TC time of associated GGA fix
    2. Total RMS standard deviation of ranges inputs to the navigation solution
    3. Standard deviation (meters) of semi-major axis of error ellipse
    4. Standard deviation (meters) of semi-minor axis of error ellipse
    5. Orientation of semi-major axis of error ellipse (true north degrees)
    6. Standard deviation (meters) of latitude error
    7. Standard deviation (meters) of longitude error
    8. Standard deviation (meters) of altitude error
    9. Checksum
    """
    example_sentence = b'$GPGST,182141.000,15.5,15.3,7.2,21.8,0.9,0.5,0.8*54'


@dataclass
class HDM(NmeaSentence):
    """
    HDM - Heading - Magnetic

           1   2 3
           |   | |
    $--HDM,x.x,M*hh<CR><LF>

    Field Number:

    1. Heading Degrees, magnetic
    2. M = magnetic
    3. Checksum
    """
    # $--HDM,x.x,M*hh<CR><LF>
    example_sentence = b'$HCHDM,140.60,M*1A'


@dataclass
class HDT(NmeaSentence):
    """
    HDT - Heading - True

    Actual vessel heading in degrees true produced by any device or system producing true heading.

           1   2 3
           |   | |
    $--HDT,x.x,T*hh<CR><LF>

    Field Number:

    1. Heading, degrees True
    2. T = True
    3. Checksum
    """
    example_sentence = b'$HEHDT,187.2,T*23'
    heading: NmeaDegree = nmea_field(1)


@dataclass
class RMC(NmeaSentence):
    """
    RMC - Recommended Minimum Navigation Information

           1         2 3       4 5        6  7   8   9   10 11 12 13
           |         | |       | |        |  |   |   |    |  | |   |
    $--RMC,hhmmss.ss,A,llll.ll,a,yyyyy.yy,a,x.x,x.x,xxxx,x.x,a,m,*hh<CR><LF>

    Field Number:

    1. UTC Time of postion
    2. Status, A = Valid, V = Warning
    3. Latitude
    4. N or S
    5. Longitude
    6. E or W
    7. Speed over ground, knots
    8. Track made good, degrees true
    9. Date, ddmmyy
    10. Magnetic Variation, degrees
    11. E or W
    12. FAA mode indicator (NMEA 2.3 and later)
    13. Checksum
    """
    example_sentence = b'$GPRMC,120557.00,A,5412.576000,N,01204.031399,E,18.9,191.3,160418,,,A*6A'
    time: NmeaTime = nmea_field(1, b'%02.0f%02.0f%05.2f')
    lat: NmeaLatitude = nmea_field(3)
    lon: NmeaLongitude = nmea_field(5)


@dataclass
class ROT(NmeaSentence):
    """
    ROT - Rate Of Turn

           1   2 3
           |   | |
    $--ROT,x.x,A*hh<CR><LF>

    Field Number:

    1. Rate Of Turn, degrees per minute, "-" means bow turns to port
    2. Status, A means data is valid
    3. Checksum
    """
    # $--ROT,x.x,A*hh<CR><LF>
    example_sentence = b'$HEROT,0.0,A*2B'
    rot: NmeaFloat = nmea_field(1)


@dataclass
class TLB(NmeaSentence):
    """
    TLB - Target Label

           1   2   (3) (4)               5
           |   |    |   |                |
    $--TLB,x.x,c--c,x.x,c--c,...,x.x,c--c*hh<CR><LF>

    Field Number:

    1. Target Number (0-99)
    2. Label assigned to target
    (3. Target number 2)
    (4. Label assigned to target number 2)
    5. Checksum

    Message can contain target number + label pairs up to maximum NMEA message length.
    Target number references to target number in TTM (and/or TLL) messages.
    """
    example_sentence = b'$RATLB,1,CHANGED*3A'
    target_id: NmeaInt = nmea_field(1)
    label: NmeaBytes = nmea_field(2)


@dataclass
class TLL(NmeaSentence):
    """
    TLL - Target Latitude and Longitude

           1  2       3 4        5 6    7         8 9 10
           |  |       | |        | |    |         | | |
    $--TLL,xx,llll.ll,a,yyyyy.yy,a,c--c,hhmmss.ss,a,a*hh<CR><LF>

    Field Number:

    1. Target Number (0-99)
    2. Target Latitude
    3. N=north, S=south
    4. Target Longitude
    5. E=east, W=west
    6. Target name
    7. UTC of data
    8. Status (L=lost, Q=acquisition, T=tracking)
    9. R= reference target; null (,,)= otherwise
    """
    example_sentence = b'$RATLL,11,5556.70,N,02143.60,E,TARGET-TLL,,T,*6A'
    target_id: NmeaInt = nmea_field(1)
    lat: NmeaFloat = nmea_field(2)
    lon: NmeaFloat = nmea_field(4)
    label: NmeaBytes = nmea_field(6)


@dataclass
class TTM(NmeaSentence):
    """
    TTM - Tracked Target Message

           1  2   3   4 5   6   7 8   9  10 11  12 13 14       15 16
           |  |   |   | |   |   | |   |   | |    | | |         |  |
    $--TTM,xx,x.x,x.x,a,x.x,x.x,a,x.x,x.x,a,c--c,a,a,hhmmss.ss,a*hh<CR><LF>

    Field Number:

    1. Target Number (0-99)
    2. Target Distance from own ship
    3. Bearing from own ship (in degree)
    4. T=true, R=relative
    5. Target speed
    6. Target course (in degree)
    7. T=true, R=relative
    8. Distance CPA
    9. Time CPA
    10. Speed + Distance Unit (K=kilometers, N=knots/nautical miles, S=statute miles)
    11. Target name
    12. Status (L=lost, Q=acquisition, T=tracking)
    13. R= reference target; null (,,)= otherwise
    14. Time of data (UTC)
    15. Type, A = Auto, M = Manual, R = Reported (NMEA 3 and above)
    16. Checksum
    """
    example_sentence = b'$RATTM,1,10.25,355.2,T,23.2,90.0,T,,,N,TARGET-TTM,T,,,M*68'
    target_id: NmeaInt = nmea_field(1)
    label: NmeaBytes = nmea_field(11)


@dataclass
class VBW(NmeaSentence):
    """
    VBW - Dual Ground/Water Speed

           1   2   3 4   5   6 7   8  0 10 11
           |   |   | |   |   | |   |  |  |  |
    $--VBW,x.x,x.x,A,x.x,x.x,A,x.x,A,x.x,A*hh<CR><LF>

    Field Number:

    1. Longitudinal water speed, "-" means astern, knots
    2. Transverse water speed, "-" means port, knots
    3. Status, A = Data Valid
    4. Longitudinal ground speed, "-" means astern, knots
    5. Transverse ground speed, "-" means port, knots
    6. Status, A = Data Valid
    7. Stern traverse water speed, knots *NMEA 3 and above)
    8. Status, stern traverse water speed A = Valid (NMEA 3 and above)
    9. Stern traverse ground speed, knots *NMEA 3 and above)
    10. Status, stern ground speed A = Valid (NMEA 3 and above)
    11. Checksum
    """
    example_sentence = b'$VDVBW,04.49,0.09,V,04.49,0.09,A*46'
    water_speed: NmeaFloat = nmea_field(1, b'%02.5f')
    ground_speed: NmeaFloat = nmea_field(4, b'%02.5f')

    def add_speed(self, speed_diff):
        self.water_speed += speed_diff
        self.ground_speed += speed_diff


@dataclass
class VTG(NmeaSentence):
    """
    VTG - Track made good and Ground speed

            1  2  3  4  5  6  7  8 9   10
            |  |  |  |  |  |  |  | |   |
    $--VTG,x.x,T,x.x,M,x.x,N,x.x,K,m,*hh<CR><LF>

    Field Number:

    1. Course over ground, degrees True
    2. T = True
    3. Course over ground, degrees Magnetic
    4. M = Magnetic
    5. Speed over ground, knots
    6. N = Knots
    7. Speed over ground, km/hr
    8. K = Kilometers Per Hour
    9. FAA mode indicator (NMEA 2.3 and later)
    10. Checksum
    """
    example_sentence = b'$GPVTG,151.253,T,151.253,M,5.416,N,10.030,K,D*12'
    course: NmeaDegree = nmea_field(1, b'%.3f')
    magnetic_course: NmeaDegree = nmea_field(3, b'%.3f')
    speed: NmeaFloat = nmea_field(5, b'%.3f')
    speed_kph: NmeaFloat = nmea_field(7, b'%.3f')

    def set_courses(self, true_course):
        """
        Convenience method to set the course in all fields consistently.
        """
        self.course = true_course
        self.magnetic_course = true_course

    def set_speeds(self, speed_knots):
        """
        Convenience method to set the speed in all units consistently.
        """
        self.speed = speed_knots
        self.speed_kph = speed_knots * 1.852


@dataclass
class ZDA(NmeaSentence):
    """
    ZDA - Time & Date - UTC, day, month, year and local time zone

    This is one of the sentences commonly emitted by GPS units.

            1         2  3  4    5  6  7
            |         |  |  |    |  |  |
     $--ZDA,hhmmss.ss,xx,xx,xxxx,xx,xx*hh<CR><LF>

    Field Number:

    1. UTC time (hours, minutes, seconds, may have fractional subsecond)
    2. Day, 01 to 31
    3. Month, 01 to 12
    4. Year (4 digits)
    5. Local zone description, 00 to +- 13 hours
    6. Local zone minutes description, 00 to 59, apply same sign as local hours
    7. Checksum
    """
    example_sentence = b'$GPZDA,072519.05,06,02,2019,01,00*64'
    time: NmeaTime = nmea_field(1)
