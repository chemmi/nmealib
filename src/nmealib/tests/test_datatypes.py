from nmealib.datatypes import NmeaTime, NmeaGeoDegreeBase, NmeaLatitude, NmeaLongitude, NmeaDegree, NmeaFloat, NmeaInt, \
    NmeaBytes

from nmealib.tests.test_sentences import float_equal


class TestNmeaBytes:
    def test_init(self):
        NmeaBytes(b'')

    def test_bytes_format(self):
        bs = NmeaBytes(b'TEST')
        assert bytes(bs) == b'TEST'

    def test_other_bytes_format(self):
        bs = NmeaBytes(b'TEST', format=b'MORE%s')
        assert bytes(bs) == b'MORETEST'


class TestNmeaInt:
    def test_init(self):
        d = NmeaInt(b'42')
        assert d == 42

    def test_bytes_format(self):
        d = NmeaInt(b'42')
        assert bytes(d) == b'42'

    def test_other_bytes_format(self):
        d = NmeaInt(b'42', format=b'%05d')
        assert bytes(d) == b'00042'


class TestNmeaFloat:
    def test_init(self):
        f = NmeaFloat(b'13.4')
        assert float_equal(f, 13.4)

    def test_bytes_format(self):
        f = NmeaFloat(b'13.4')
        assert bytes(f) == b'13.4'

    def test_other_bytes_format(self):
        f = NmeaFloat(b'13.4', format=b'%.2f')
        assert bytes(f) == b'13.40'


class TestNmeaTime:
    def test_init(self):
        t = NmeaTime(b'000000')
        assert float_equal(0.0, float(t))
        t = NmeaTime(1)
        assert float_equal(1, float(t))

    def test_constructor(self):
        t = NmeaTime(b'121011.11')
        time = 11.11 + 60 * 10 + 3600 * 12
        assert float_equal(float(t), time)

    def test_add_seconds(self):
        t = NmeaTime(b'000005')
        t += 2
        assert float_equal(float(t), 7)

    def test_nmea_representation(self):
        t = NmeaTime(b'121011.11')
        t += 3 * 60 + 4 * 3600
        assert bytes(t) == b'161311.11'

    def test_overnight(self):
        t = NmeaTime(b'235959')
        t += 2
        assert bytes(t) == b'000001.00'
        t = NmeaTime(b'000001')
        t -= 2
        assert bytes(t) == b'235959.00'

    def test_format_no_milli_seconds(self):
        t = NmeaTime(b'121011.11', format=b'%02.0f%02.0f%02.0f')
        assert bytes(t) == b'121011'


class TestNmeaDegree:
    def test_init(self):
        d = NmeaDegree(b'180.1')
        assert float_equal(d, 180.1)

    def test_overflow_init(self):
        d = NmeaDegree(361)
        assert float_equal(d, 1)

    def test_overflow(self):
        d = NmeaDegree(359)
        d += 2
        assert d < 360

    def test_underflow(self):
        d = NmeaDegree(1)
        d -= 2
        assert d > 0

    def test_bytes_format(self):
        d = NmeaDegree(180)
        assert bytes(d) == b'180.0'

    def test_other_format(self):
        d = NmeaDegree(180, format=b'%.2f')
        assert bytes(d) == b'180.00'


class TestNmeaGeoDegree:
    def test_init(self):
        d = NmeaGeoDegreeBase(b'0001.00')
        assert float_equal(d, 1)

    def test_minutes_is_internal(self):
        d = NmeaGeoDegreeBase(b'0100.01')
        assert float_equal(d, 60.01)

    def test_negative_degrees(self):
        d = NmeaGeoDegreeBase(b'-100.01')
        assert float_equal(d, -60.01)

    def test_addition_substraction(self):
        d = NmeaGeoDegreeBase(b'0100.01')
        d += 125
        d -= 5
        assert float_equal(d, 180.01)

    def test_representation(self):
        lat = NmeaLatitude(b'4735.5634')
        assert (b'%s' % lat) == b'4735.5634'
        lon = NmeaLongitude(b'00739.3538')
        assert (b'%s' % lon) == b'00739.3538'

    def test_other_format(self):
        d = NmeaGeoDegreeBase(0.0, format=b'%02.0f')
        assert bytes(d) == b'00'
