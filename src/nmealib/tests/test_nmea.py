from dataclasses import dataclass
from typing import ClassVar

from nmealib.datatypes import NmeaFloat
from nmealib.nmea import NmeaSentence, nmea_field


def float_equal(x, y, d=0.01):
    return abs(x - y) < d


class TestNmeaSentence:
    def test_init(self):
        NmeaSentence(b'$IDSEN,A,B,C*39\r\n')

    def test_get_field(self):
        n = NmeaSentence(b'$IDSEN,A,B,C*39\r\n')
        assert n.get_field(1) == b'A'

    def test_set_field(self):
        n = NmeaSentence(b'$IDSEN,A,B,C*39\r\n')
        n.set_field(1, b'NEW')
        assert n.get_field(1) == b'NEW'

    def test_to_bytes(self):
        n = NmeaSentence(b'$IDSEN,A,B,C*39\r\n')
        assert bytes(n) == b'$IDSEN,A,B,C*39\r\n'

    def test_auto_checksum(self):
        n = NmeaSentence(b'$IDSEN,A,B,C*00\r\n')
        assert bytes(n) == b'$IDSEN,A,B,C*39\r\n'


@dataclass
class EXA(NmeaSentence):
    ex_value: NmeaFloat = nmea_field(1)


class TestExampleNmeaSentence:
    def test_integrity(self):
        n = EXA(b'$IDEXA,0.0*53\r\n')
        assert bytes(n) == b'$IDEXA,0.0*53\r\n'

    def test_conv_field(self):
        n = EXA(b'$IDEXA,0.0*53\r\n')
        assert float_equal(n.ex_value, 0)

    def test_set_field(self):
        n = EXA(b'$IDEXA,0.0*53\r\n')
        n.ex_value = 1
        assert float_equal(n.ex_value, 1)

    def test_set_conv_field_changes_sentence(self):
        n = EXA(b'$IDEXA,0.0*53\r\n')
        n.ex_value = 1.0
        assert b'1.0' in bytes(n)

    def test_set_field_overwrites_set_conv_field(self):
        n = EXA(b'$IDEXA,0.0*53\r\n')
        n.ex_value = 1.0
        n.set_field(1, b'2.0')
        assert b'2.0' in bytes(n)

    def test_set_field_changes_conv_field(self):
        n = EXA(b'$IDEXA,0.0*53\r\n')
        n.set_field(1, b'2.0')
        assert float_equal(n.ex_value, 2)

    def test_auto_typing(self):
        n = EXA(b'$IDEXA,0.0*53\r\n')
        n.ex_value = 3
        assert isinstance(n.ex_value, NmeaFloat)

    def test_init_empty_field(self):
        n = EXA(b'$IDEXA,*53\r\n')
        assert n.get_field(1) == b''
        assert n.ex_value is None

    def test_set_empty_field(self):
        n = EXA(b'$IDEXA,0.0*53\r\n')
        n.set_field(1, b'')
        assert n.ex_value is None

    def test_set_empty_conv_field(self):
        n = EXA(b'$IDEXA,0.0*53\r\n')
        n.ex_value = None
        assert n.get_field(1) == b''

    def test_set_conv_field_of_right_nmea_type(self):
        n = EXA(b'$IDEXA,0.0*53\r\n')
        value = NmeaFloat(23)
        assert isinstance(value, NmeaFloat)
        n.ex_value = value
        assert b'23.0' in bytes(n)
        assert isinstance(n.ex_value, NmeaFloat)

    def test_conv_field_init_parsing_not_possible(self):
        n = EXA(b'$IDEXA,notparsable*53\r\n')
        assert n.ex_value is None
        assert n.get_field(1) == b'notparsable'

    def test_conv_field_parsing_not_possible(self):
        n = EXA(b'$IDEXA,0.0*53\r\n')
        n.set_field(1, b'notparsable')
        assert n.ex_value is None
        assert n.get_field(1) == b'notparsable'

    def test_overwrite_conv_fields_in_constructor(self):
        n = EXA(b'$IDEXA,0.0*53\r\n', ex_value=2.0)
        assert float_equal(n.ex_value, 2)
        assert isinstance(n.ex_value, NmeaFloat)
        assert n.get_field(1) == b'2.0'

    def test_conv_names_in_set_and_get_field(self):
        n = EXA(b'$IDEXA,0.0*53\r\n')
        n.set_field('ex_value', b'2.0')
        assert n.get_field('ex_value') == b'2.0'

    def test_replace(self):
        n1 = EXA(b'$IDEXA,0.0*53\r\n')
        n2 = n1.replace(ex_value=2.0)
        assert b'2.0' in bytes(n2)
        assert bytes(n1) == b'$IDEXA,0.0*53\r\n'


@dataclass
class EXB(NmeaSentence):
    ex_value: NmeaFloat = nmea_field(1, format=b'%.3f')


class TestEXB:
    def test_other_nmea_type_format(self):
        n = EXB(b'$IDEXB,1.0*53\r\n')
        n.ex_value = 2
        assert b'2.000' in bytes(n)


@dataclass
class EXC(NmeaSentence):
    example_sentence: ClassVar[bytes] = b'$IDEXC,1.0*53\r\n'


class TestEXC:
    def test_init_with_example_class(self):
        EXC()
