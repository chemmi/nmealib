from nmealib.datatypes import NmeaLatitude
from nmealib.sentences import DBT, DPT, DRU, GGA, GLL, GST, HDM, HDT, RMC, ROT, TLB, TLL, TTM, VBW, VTG, ZDA


def float_equal(x, y, d=0.01):
    return abs(x - y) < d


class TestDBT:
    def test_manipulate_depth(self):
        dbt = DBT()
        depth = dbt.depth
        dbt.depth += 5
        assert str(depth + 5).encode() in bytes(dbt)

    def test_adjust_other_depths(self):
        dbt = DBT()
        depth = dbt.depth
        dbt.set_depths(dbt.depth + 5)
        depth_meters = depth + 5
        depth_feet = int(depth_meters * 3.28084)
        depth_fathoms = int(depth_meters * 0.546807)
        assert str(depth_meters).encode() in bytes(dbt)
        assert str(depth_feet).encode() in bytes(dbt)
        assert str(depth_fathoms).encode() in bytes(dbt)

    def test_skip_unit_if_not_set(self):
        dbt = DBT(b'$SDDBT,,f,050.79,M,,F*3A\r\n')
        dbt.set_depths(55)
        assert float_equal(dbt.depth, 55)
        assert dbt.get_field('depth_feet') == b''
        assert dbt.get_field('depth_fathoms') == b''


class TestDPT:
    def test_manipulate_depth_offset(self):
        dpt = DPT()
        depth, offset = dpt.depth, dpt.offset
        dpt.depth += 5
        dpt.offset += 6
        assert str(depth + 5).encode() in bytes(dpt)
        assert str(int(offset + 6)).encode() in bytes(dpt)


class TestDRU:
    def test_parse(self):
        DRU(b'$PKDRU,0003.9,M,,,+000,*3E')
        dru = DRU()
        assert b'DRU' in bytes(dru)


class TestGGA:
    def test_manipulate_lat_lon(self):
        gga = GGA()
        lat, lon = gga.lat, gga.lon
        gga.lat -= 9
        gga.lon += 12
        gga2 = GGA(bytes(gga))
        assert float_equal(gga2.lat, lat - 9)
        assert float_equal(gga2.lon, lon + 12)

    def test_time(self):
        gga = GGA(b'$PwGGA,191410,4735.5634,N,00739.3538,E,1,04,4.4,351.5,M,48.0,M,,*45')
        assert b'191410' in bytes(gga.time)

    def test_replace(self):
        gga1 = GGA()
        gga2 = gga1.replace()
        assert gga1 == gga2
        assert bytes(gga1) == bytes(gga2)
        gga1.lat += 10
        assert gga1 != gga2

    def test_replace_directly(self):
        gga1 = GGA()
        lat = NmeaLatitude(b'1337')
        gga2 = gga1.replace(lat=lat)
        assert gga2.lat == lat
        gga2.lat += 1
        assert gga2.lat != lat

    def test_replace_with_non_default_template(self):
        gg = b'$PwGGA,191410,4735.5634,N,00739.3538,E,1,04,4.4,351.5,M,48.0,M,,*45'
        gga1 = GGA(bytes(gg))
        gga2 = gga1.replace()
        assert gga1 == gga2
        assert bytes(gga1) == bytes(gga2)
        gga1.lat += 10
        assert bytes(gga1) != bytes(gga2)


class TestGLL:
    def test_manipulate_lat_lon(self):
        gll = GLL()
        lat, lon = gll.lat, gll.lon
        gll.lat -= 9
        gll.lon += 12
        gll2 = GLL(bytes(gll))
        assert float_equal(gll2.lat, lat - 9)
        assert float_equal(gll2.lon, lon + 12)

    def test_add_seconds(self):
        gll = GLL(b'$GPGLL,4735.5634,N,00739.3538,E,191410,A,A*4A')
        assert gll.time
        gll.time += 60
        assert b'191510' in bytes(gll)


class TestGST:
    def test_parse(self):
        GST(b'$GPGST,182141.000,15.5,15.3,7.2,21.8,0.9,0.5,0.8*54')
        gst = GST()
        assert b'GST' in bytes(gst)


class TestHDM:
    def test_parse(self):
        HDM(b'$HCHDM,140.60,M*1A')
        hdm = HDM()
        assert b'HDM' in bytes(hdm)


class TestHDT:
    def test_manipulate_heading(self):
        hdt = HDT()
        heading = hdt.heading
        hdt.heading += 9
        assert str(int(heading + 9) % 360).encode() in bytes(hdt)


class TestRMC:
    def test_time(self):
        rmc = RMC(b'$GPRMC,120557.00,A,5412.576000,N,01204.031399,E,18.9,191.3,160418,,,A*6A')
        assert bytes(rmc.time).startswith(b'120557')

    def test_manipulate_lat_lon(self):
        rmc = RMC(b'$GPRMC,120557.00,A,5412.576000,N,01204.031399,E,18.9,191.3,160418,,,A*6A')
        rmc.lat -= 9
        rmc.lon += 12
        assert b'5403' in bytes(rmc)
        assert b'1216' in bytes(rmc)


class TestROT:
    def test_init(self):
        assert b'ROT' in bytes(ROT())
        rot = ROT(b'$HEROT,0.3,A*2B')
        assert float_equal(rot.rot, 0.3)


class TestTLB:
    def test_init(self):
        assert b'TLB' in bytes(TLB())
        tlb = TLB(b'$RATLB,1,CHANGED*3A')
        assert tlb.target_id == 1
        assert tlb.label == b'CHANGED'

    def test_change_target_id_and_label(self):
        tlb = TLB()
        tlb.target_id = 99
        tlb.label = b'TARGET_LABEL'
        assert b'TLB,99,TARGET_LABEL*' in bytes(tlb)


class TestTLL:
    def test_init(self):
        assert b'TLL' in bytes(TLL())
        tll = TLL(b'$RATLL,11,5556.70,N,02143.60,E,TARGET-TLL,,T,*6A')
        assert tll.target_id == 11
        assert tll.label == b'TARGET-TLL'
        assert abs(tll.lat - 5556) < 1
        assert abs(tll.lon - 2143) < 1


class TestTTM:
    def test_init(self):
        assert b'TTM' in bytes(TTM())
        ttm = TTM(b'$RATTM,1,10.25,355.2,T,23.2,90.0,T,,,N,TARGET-TTM,T,,,M*68')
        assert ttm.target_id == 1
        assert ttm.label == b'TARGET-TTM'


class TestVBW:
    def test_init(self):
        assert b'VBW' in bytes(VBW())
        vbw = VBW(b'$VDVBW,04.49,0.09,V,05.49,0.09,A*46')
        assert float_equal(vbw.water_speed, 4.49)
        assert float_equal(vbw.ground_speed, 5.49)

    def test_change_speeds(self):
        vbw = VBW(b'$VDVBW,04.49,0.09,V,05.49,0.09,A*46')
        vbw.add_speed(20)
        assert float_equal(vbw.water_speed, 24.49)
        assert float_equal(vbw.ground_speed, 25.49)


class TestVTG:
    def test_init(self):
        assert b'VTG' in bytes(VTG())
        vtg = VTG(b'$GPVTG,151.253,T,151.253,M,5.416,N,10.030,K,D*12')
        assert float_equal(vtg.course, 151.253)
        assert float_equal(vtg.speed, 5.416)

    def test_change_both_courses(self):
        vtg = VTG(b'$GPVTG,151.253,T,151.253,M,5.416,N,10.030,K,D*12')
        vtg.set_courses(vtg.course + 10)
        assert float_equal(vtg.course, 161.253)
        assert float_equal(vtg.magnetic_course, 161.253)

    def test_change_both_speeds(self):
        vtg = VTG(b'$GPVTG,151.253,T,151.253,M,5.416,N,10.030,K,D*12')
        vtg.set_speeds(vtg.speed + 10)
        vtg = VTG(bytes(vtg))
        assert float_equal(vtg.speed, 15.416)
        assert float_equal(vtg.speed_kph, 28.550)

    def test_right_conversion(self):
        vtg = VTG(b'$GPVTG,151.253,T,151.253,M,5.416,N,10.030,K,D*12')
        vtg = VTG(bytes(vtg))
        assert float_equal(vtg.speed_kph, 10.030)
        assert float_equal(vtg.speed, 5.416)


class TestZDA:
    def test_init(self):
        assert b'ZDA' in bytes(ZDA())
        zda = ZDA(b'$GPZDA,072519.05,06,02,2019,01,00*64')
        assert zda.time

    def test_time(self):
        zda = ZDA(b'$GPZDA,072519.05,06,02,2019,01,00*64')
        assert bytes(zda.time) == b'072519.05'
