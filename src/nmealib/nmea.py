import binascii
import operator
from dataclasses import dataclass, InitVar, fields, field as dataclass_field, Field, replace
from functools import reduce
from typing import Any, List, Union, ClassVar


def nmea_field(idx, format=None):
    return dataclass_field(metadata={'nmealib_idx': idx, 'nmealib_format': format}, default=None)


@dataclass
class NmeaSentence:
    # TODO: An example sentence for the base class does not make sense?
    example_sentence: ClassVar[bytes] = None  # b'$*00\r\n'
    sentence: InitVar[bytes] = None
    _post_init = False

    def __post_init__(self, sentence=None):
        sentence = sentence or self.example_sentence
        self._fields = {field.metadata["nmealib_idx"]: field for field in fields(self)}
        self._fields_name_to_idx = {field.name: idx for idx, field in self._fields.items()}
        # This switches the behavior of _setattr_
        self._post_init = True
        # Fill the fields
        sentence_fields = sentence.split(b'*')[0][1:].split(b',')
        self._raw_fields: List[bytes] = [None] * len(sentence_fields)
        for idx, raw_value in enumerate(sentence_fields):
            try:
                name = self._fields[idx].name
            except KeyError:
                self.set_field(idx, raw_value)
            else:
                value = self.__getattribute__(name)
                if value is not None:
                    self.__setattr__(name, value)
                else:
                    self.set_field(idx, raw_value)

    def get_field(self, idx: Union[int, str]) -> bytes:
        if isinstance(idx, str):
            idx = self._fields_name_to_idx[idx]
        return self._get_raw_field(idx)

    def set_field(self, idx: Union[int, str], raw_value: bytes) -> None:
        if isinstance(idx, str):
            idx = self._fields_name_to_idx[idx]
        try:
            field = self._fields[idx]
        except KeyError:
            pass
        else:
            if not raw_value == b'':
                try:
                    parsed_value = self._parse_value(raw_value, field)
                except ValueError:
                    self.__setattr__(field.name, None)
                else:
                    self.__setattr__(field.name, parsed_value)
            else:
                self.__setattr__(field.name, None)
        finally:
            self._set_raw_field(idx, raw_value)

    def __setattr__(self, name: str, value: Any) -> None:
        # Change behavior after initialization of field list
        if not self._post_init:
            super().__setattr__(name, value)
        if self._post_init:
            try:
                idx = self._fields_name_to_idx[name]
            except KeyError:
                super().__setattr__(name, value)
            else:
                field = self._fields[idx]
                if value is not None:
                    parsed_value = self._parse_value(value, field)
                    super().__setattr__(name, parsed_value)
                    self._set_raw_field(idx, bytes(parsed_value))
                else:
                    super().__setattr__(name, None)
                    self._set_raw_field(idx, b'')

    def _parse_value(self, value: Union[Any, bytes], field: Field):
        try:
            format_ = field.metadata['nmealib_format']
        except KeyError:
            return field.type(value)
        else:
            return field.type(value, format=format_)

    def _get_raw_field(self, idx: int) -> bytes:
        return self._raw_fields[idx]

    def _set_raw_field(self, idx: int, value: bytes) -> None:
        self._raw_fields[idx] = value

    def __bytes__(self) -> bytes:
        data = b','.join(self._raw_fields)
        return b'$' + data + b'*' + self.checksum(data) + b'\r\n'

    @staticmethod
    def checksum(data):
        return binascii.b2a_hex(bytes((reduce(operator.xor, data, 0),)))

    def replace(self, **kwargs):
        return replace(self, sentence=bytes(self), **kwargs)
