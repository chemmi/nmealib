# Subclass builtin types
# https://stackoverflow.com/questions/35943789/python-can-a-subclass-of-float-take-extra-arguments-in-its-constructor


class NmeaBytes(bytes):
    default_format = b'%s'

    def __new__(cls, value=b'', format=None):
        return bytes.__new__(cls, value)

    def __init__(self, value=b'', format=None):
        bytes.__init__(value)
        self.format = format

    def __bytes__(self):
        format_ = self.format or self.default_format
        return format_ % self


class NmeaInt(int):
    default_format = b"%d"

    def __new__(cls, value=0, format=None):
        return int.__new__(cls, value)

    def __init__(self, value=0, format=None):
        int.__init__(value)
        self.format = format

    def __bytes__(self):
        format_ = self.format or self.default_format
        return format_ % self


class NmeaFloat(float):
    default_format = b"%.1f"

    def __new__(cls, value=0.0, format=None):
        return float.__new__(cls, value)

    def __init__(self, value=0.0, format=None):
        float.__init__(value)
        self.format = format

    def __bytes__(self):
        format_ = self.format or self.default_format
        return format_ % self


class NmeaTime(float):
    default_format = b'%02.0f%02.0f%05.2f'

    def __new__(cls, value=0.0, format=None):
        if isinstance(value, bytes):
            value = cls._bytes_to_float(value)
        return float.__new__(cls, value)

    def __init__(self, value=0.0, format=None):
        if isinstance(value, bytes):
            value = self._bytes_to_float(value)
        float.__init__(value)
        self.format = format

    @staticmethod
    def _bytes_to_float(bs):
        hours = float(bs[0:2])
        minutes = float(bs[2:4])
        seconds = float(bs[4:])
        return hours * 3600 + minutes * 60 + seconds

    def __add__(self, other):
        return NmeaTime(float(self) + other)

    def __sub__(self, other):
        return NmeaTime(float(self) - other)

    def __bytes__(self):
        seconds = self % (60 * 60 * 24)
        minutes = seconds // 60
        seconds = seconds % 60
        hours = minutes // 60
        minutes = minutes % 60
        format_ = self.format or self.default_format
        return format_ % (hours, minutes, seconds)


class NmeaDegree(float):
    default_format = b"%.1f"

    def __new__(cls, value=0.0, format=None):
        if isinstance(value, bytes):
            value = cls._bytes_to_float(value)
        value %= 360
        return float.__new__(cls, value)

    def __init__(self, value=0.0, format=None):
        if isinstance(value, bytes):
            value = self._bytes_to_float(value)
        value %= 360
        float.__init__(value)
        self.format = format

    @staticmethod
    def _bytes_to_float(bs):
        return float(bs)

    def __add__(self, other):
        cls = self.__class__
        return cls(float(self) + other)

    def __sub__(self, other):
        cls = self.__class__
        return cls(float(self) - other)

    def __bytes__(self):
        format_ = self.format or self.default_format
        return format_ % self


class NmeaGeoDegreeBase(float):
    # Value is in minutes
    default_format = b"%09.4f"

    def __new__(cls, value=0.0, format=None):
        if isinstance(value, bytes):
            value = cls._bytes_to_float(value)
        return float.__new__(cls, value)

    def __init__(self, value=0.0, format=None):
        if isinstance(value, bytes):
            value = self._bytes_to_float(value)
        float.__init__(value)
        self.format = format

    @staticmethod
    def _bytes_to_float(bs):
        sign = -1 if float(bs) < 0 else 1
        ddmm = abs(float(bs))
        degrees = ddmm // 100
        minutes = ddmm % 100
        return float(sign * (degrees * 60 + minutes))

    def __add__(self, other):
        cls = self.__class__
        return cls(float(self) + other)

    def __sub__(self, other):
        cls = self.__class__
        return cls(float(self) - other)

    def __bytes__(self):
        minutes = float(self)
        sign = -1 if minutes < 0 else 1
        minutes = abs(minutes)
        degree = minutes // 60
        minutes = minutes % 60
        format_ = self.format or self.default_format
        return format_ % (sign * ((degree * 100) + minutes))


class NmeaLatitude(NmeaGeoDegreeBase):
    # 4735.5634,N
    default_format = b"%09.4f"


class NmeaLongitude(NmeaGeoDegreeBase):
    # 00739.3538,E
    default_format = b"%010.4f"
