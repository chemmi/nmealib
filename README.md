# nmealib

This is a python library to work with sentences defined in the NMEA 0183 protocol.
In contrast to other libraries for NMEA sentences (e.g. pynmea2), this one focuses on convenient and explicit manipulation of sentences (beyond the protocol specification).
For instance, this can be very useful when testing maritime systems for security.
